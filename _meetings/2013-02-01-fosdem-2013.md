---
layout: single
title: "FOSDEM 2013"
date: 2013-02-01
permalink: /fosdem-2013
categories:
    - meetings
---

The face-to-face meetings are a place for the people who try to Get Stuff Done at various open source projects and non-profits around open source to congregate, share information, and pick each others' brains.

The restaurant is now heading towards full. I've created a standby list - basically, if you're not on the list and want to eat with us, please let me know, turn up, have a drink with us before dinner, and if there's a spare seat at 8:30 (or we can fit in an extra two or three chairs) you're welcome to stay.

## When and Where:

Friday 8:00pm, restaurant Les Chapeliers, Rue des Chapeliers 3, 1000 Brussels, Belgium (we'll order food at 8:30pm) at [FOSDEM 2013](http://fosdem.org/2013/).

Attendee List

* Loïc Dachary, April
* Donnie Berkholz, Gentoo
* Gervase Markham, Mozilla
* Michael Meeks, LibreOffice
* Lydia Pintscher, KDE
* Jean-Baptiste Kempf, VideoLAN
* Jeremy Allison, Google
* Stefano Zacchiroli, Debian
* Ruth Suehle, Red Hat
* Deborah Nicholson, OpenHatch
* Louis Suárez-Potts
* Simon Phipps, OSI/ORG (+ Alexandra Combes, OIN/Ultimate Circle)
* Justin Erenkrantz, Apache
* Rich Sands, Ohloh/Black Duck
* Charles-H. Schulz/The Document Foundation (+1)
* Matt Zimmerman, Ubuntu/The Ada Initiative
* Dave Neary, GNOME, Red Hat
* Frederic Couchet, APRIL (maybe)
* Lance Albertson, Gentoo/OSU OSL
* Chris Aniszczyk, Twitter
* Björn Balasz
* John Sullivan, FSF (maybe)
* Leslie Hawthorn, Red Hat (+1)
* Bradley M. Kuhn, Conservancy
* Robert Lemke, TYPO3 (+1)
* Martin Michlmayr, Debian
* Mike Milinkovich, Eclipse
* Ian Skerritt, Eclipse
* Amanda Brock
* Andrew Katz
* Cat Allman, Google
* Richard Fontana, Red Hat
* Pam Chestek
* Patrick Reilly, Wikimedia Foundation

Standby List

* Benjamin Henrion, FFII

## Topics:

* Food
* Drink
* Camaraderie
* Others?

Thank you all who can attend. Please contribute your notes as appropriate.
