---
title: Foundation Directory
layout: single
permalink: /foundation-directory/
---

_We thank Michael Dexter of Linux Fund for compiling the original list._

Jump to: [Defunct Organisations](#defunct-organisations), [Definitions](#definitions)

[#](#0-9) [A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## Active Organisations

<a name="0-9"></a>
* .NET Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://dotnetfoundation.org/">https://dotnetfoundation.org</a>
    * The .NET Foundation is an independent, non-profit organization established to support an innovative, commercially friendly, open-source ecosystem around the .NET platform.

* 3MF Consortium
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://3mf.io">https://3mf.io</a>
    * 3MF is an industry consortium working to define a 3D printing format that will allow design applications to send full-fidelity 3D models to a mix of other applications, platforms, services and printers. Our goal is to provide a specification that eliminates the issues with currently available file formats, and allows companies to focus on innovation, rather than on basic interoperability issues.

<a name="a"></a>
* Academy Software Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.aswf.io">https://www.aswf.io</a>
    * The Academy Software Foundation (ASWF) was founded in August 2018 by the Academy of Motion Picture Arts & Sciences (AMPAS) as a result of a two-year survey by the Science and Technology Council into the use of Open Source Software (OSS) across the motion picture industry. The survey found that almost 84% of the industry uses open source software, particularly for animation and visual effects, but challenges including siloed development, managing multiple versions of OSS libraries (versionitis) and varying governance and licensing models need to be addressed in order to ensure a healthy open source community.

* Accord Project
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://accordproject.org">https://accordproject.org</a>
    * The Accord Project is a non-profit, collaborative, initiative developing an ecosystem and open source tools for smart legal contracts. Open source means that anyone can freely use and contribute to development.

* Alliance for Open Media
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="http://aomedia.org">http://aomedia.org</a>
    * AOMedia unites top tech leaders behind a collaborative effort to offer open, royalty-free and interoperable solutions for the next generation of media delivery. The Alliance's shared vision is to make media technology more efficient, cost-effective and of superior quality for all users, on all devices, and on all platforms using AOM standards & tools.

* AlmaLinux OS Foundation
    * Type: Independant
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://almalinux.org">https://almalinux.org</a>
    * AlmaLinux OS is a 1:1 binary compatible clone of RHEL®, guided and built by the community. We were the first downstream RHEL clone to be released after the RedHat announced the CentOS Linux was ending, and it is our goal to provide an option for folks who have historically used CentOS Linux, and for whom CentOS Stream is not an option. The AlmaLinux OS Foundation is a non-profit created for the benefit of the AlmaLinux OS community. 

* Apache Software Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://www.apache.org/foundation/">http://www.apache.org/foundation/</a>
    * The mission of the Apache Software Foundation (ASF) is to provide software for the public good. We do this by providing services and support for many like-minded software project communities consisting of individuals who choose to participate in ASF activities.

* Associação Python Brasil
    * Type: Independent
    * Legal/financial status: Brazilian NGO
    * <a href="https://python.org.br/apyb/">https://python.org.br/apyb/</a>
    * Associação Python Brasil (APyB) was formed in April 2007 with the goal of supporting communities related to the Python language and its derived technologies.

* Associação SoftwareLivre.org
    * Type: Independent
    * Legal/financial status: Brazilian NGO
    * <a href="http://softwarelivre.org/asl">http://softwarelivre.org/asl</a>
    * A Associação SoftwareLivre.org (ASL) é uma associação civil sem fins-lucrativos, com sede em Porto Alegre/RS que reune empresários, profissionais liberais, estudantes e servidores públicos, estabelecendo relações com os mais diversos setores da sociedade como o poder público, universidades, empresas, grupos de usuários, hackers e ONGs. A ASL tem por principal objetivo tornar o software livre amplamente incluído na sociedade, propiciando espaço de discussão, apoio, fomento e organização de iniciativas nas mais diversas áreas relacionadas.

<a name="b"></a>
* Benetech
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://www.benetech.org/">http://www.benetech.org/</a>
    * Our work transforms how people with disabilities read and learn, makes it safer for human rights defenders to pursue truth and justice, and connects people to the services they need to live and prosper. We’re constantly pursuing the next big social impact. Our mission is to empower communities with software for social good. We achieve our mission by uniting two worlds: the social sector and Silicon Valley. We serve as a bridge between the social sector and Silicon Valley by working closely with both communities to identify needs and software solutions that can drive positive social change.

* BioBricks Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://biobricks.org">https://biobricks.org</a>
    * The BioBricks Foundation is a public-benefit organization advancing synthetic biology to benefit all people and the planet. We have an impact because of people like you — you are one of a community of thousands of scientists, educators, policymakers, students, and citizens who believe fundamental biotechnology belongs to all of us.
 
* Blender Foundation
    * Type: Independent
    * Legal/financial status: Dutch Stichting
    * <a href="https://www.blender.org/foundation/">https://www.blender.org/foundation/</a>
    * The Blender Foundation (2002) is an independent public benefit organization with the purpose to provide a complete, free and open source 3D creation pipeline, managed by public projects on blender.org.
 
* BSD Fund
    * Type: Independent
    * Legal/financial status: Trademark of Gainframe LLC, a United States for-profit business
    * <a href="http://bsdfund.org/">http://bsdfund.org/</a>
    * BSD Fund is a reputation-based sponsorship program operated by Michael Dexter through Gainframe LLC of Oregon, United States.

* Bytecode Alliance
    * Type: Independent
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://bytecodealliance.org">https://bytecodealliance.org</a>
    * The Bytecode Alliance is a nonprofit organization dedicated to creating secure new software foundations, building on standards such as WebAssembly and WebAssembly System Interface (WASI).

<a name="c"></a>
* Center for the Cultivation of Technology
    * Type: Independent
    * Legal/financial status: German non-profit company (gGmbH)
    * <a href="https://www.techcultivation.org/">https://www.techcultivation.org</a>
    * The Center for the Cultivation of Technology is a European non-profit "backend provider" for the Free Software community. Affiliated projects use the legal entity to apply for grants, collect donations and to manage their budgets and assets, while we take care of matters such as accounting, reimbursements and financial reporting. We work with projects to identify funding opportunities, provide additional services (together with partner organizations), exchange knowledge about sustainable organizational development, and allow developers to focus on what they do best: write code.

* CHIPS Alliance
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://chipsalliance.org">https://chipsalliance.org</a>
    * The CHIPS Alliance develops high-quality, open source hardware designs relevant to silicon devices and FPGAs. By creating an open and collaborative environment, CHIPS Alliance shares resources to lower the cost of development. Companies and individuals can work together to develop open source CPUs, various peripherals, and complex IP blocks. CHIPS Alliance is open to all organizations who are interested in collaborating on open source hardware or software tools to accelerate the creation of more efficient and innovative chip designs.

* Civil Infrastructure Platform
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.cip-project.org">https://www.cip-project.org</a>
    * The Civil Infrastructure Platform (“CIP”) is a collaborative, open source project hosted by the Linux Foundation. The CIP project is focused on establishing an open source “base layer” of industrial grade software to enable the use and implementation of software building blocks in civil infrastructure projects. Currently, civil infrastructure systems are built from the ground up, with little re- use of existing software building blocks.

* Clojurists Together Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.clojuriststogether.org/">https://www.clojuriststogether.org</a>
    * The Clojurists Together Foundation is a trade organisation, dedicated to funding and supporting open source software, infrastructure, and documentation that is important to the Clojure and ClojureScript community.

* Cloud Foundry
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.cloudfoundry.org">https://www.cloudfoundry.org</a>
    * The Cloud Foundry Foundation exists to drive the global awareness and adoption of the Cloud Foundry open source project, to grow a vibrant community of contributors, and to create coherence in strategy and action across all member companies for the sake of the project. We will do this by marketing the economic and technical activity of the entire Cloud Foundry community; governing with fairness, energy, and optimism; and growing a massive ecosystem based on application and skills portability.

* Cloud Native Computing Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.cncf.io">https://www.cncf.io</a>
    * The Cloud Native Computing Foundation (CNCF) hosts critical components of the global technology infrastructure. CNCF brings together the world’s top developers, end users, and vendors and runs the largest open source developer conferences. CNCF is part of the nonprofit Linux Foundation.

* Code Aurora
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.codeaurora.org">https://www.codeaurora.org</a>
    * Code Aurora Forum, a Linux Foundation Collaborative Project, hosts tested open source software code needed to provide upstream enablement for innovative, performance optimized, network connectivity and related ecosystems to support system on a chip (SoC) products. It also serves as a staging area for open source code that is submitted to various upstream projects such as the Linux kernel and Android. CAF also mirrors key upstream projects for use by the community.

* Commons Conservancy
    * Type: Independent
    * Legal/financial status: Dutch Stichting
    * <a href="https://commonsconservancy.org/">https://commonsconservancy.org</a>
    * The Commons Conservancy is a foundation ("stichting") established under the law of The Netherlands on October 21st 2016. The organization is a home for technology efforts in the public interest.

* Community Data License Agreement <!-- May not qualify for the directory? -->
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://cdla.dev">https://cdla.dev</a>
    * Our communities wanted to develop data license agreements that could enable sharing of data similar to what we have with open source software. The result is a large scale collaboration on two licenses for sharing data under a legal framework which we call the Community Data License Agreement (CDLA).

* Confidential Computing Consortium
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://confidentialcomputing.io">https://confidentialcomputing.io</a>
    * The Confidential Computing Consortium (CCC) brings together hardware vendors, cloud providers, and software developers to accelerate the adoption of Trusted Execution Environment (TEE) technologies and standards. CCC is a project community at the Linux Foundation dedicated to defining and accelerating the adoption of confidential computing. It will embody open governance and open collaboration that has aided the success of similarly ambitious efforts. The effort includes commitments from numerous member organizations and contributions from several open source projects.

* Continuous Delivery Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://cd.foundation">https://cd.foundation</a>
    * The Continuous Delivery Foundation (CDF) serves as the vendor-neutral home of many of the fastest-growing projects for continuous integration/continuous delivery (CI/CD). It fosters vendor-neutral collaboration between the industry’s top developers, end users and vendors to further CI/CD best practices and industry specifications. Its mission is to grow and sustain projects that are part of the broad and growing continuous delivery ecosystem.

* Core Infrastructure Initiative
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.coreinfrastructure.org">https://www.coreinfrastructure.org</a>
    * The Core Infrastructure Initiative is a multi-million dollar project to fund and support critical elements of the global information infrastructure. It is organized by The Linux Foundation and supported by Amazon Web Services, Adobe, Bloomberg, Cisco, Dell, Facebook, Fujitsu, Google, Hitachi, HP, Huawei, IBM, Intel, Microsoft, NetApp, NEC, Qualcomm, RackSpace, salesforce.com, and VMware. CII enables technology companies to collaboratively identify and fund open source projects that are in need of assistance, while allowing the developers to continue their work under the community norms that have made open source so successful.
 
* Creative Commons
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://creativecommons.org/">https://creativecommons.org/</a>
    * Creative Commons is a nonprofit organization that helps overcome legal obstacles to the sharing of knowledge and creativity to address the world’s pressing challenges.

<a name="d"></a>
* Decentralized Identity Foundation (DIF)
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://identity.foundation">https://identity.foundation</a>
    * DIF is an engineering-driven organization focused on developing the foundational elements necessary to establish an open ecosystem for decentralized identity and ensure interop between all participants.

* Diamon Workgroup
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://diamon.org">https://diamon.org</a>
    * The Diagnostic and Monitoring workgroup (DiaMon) aims to improve open-source diagnostic and monitoring software.
 
* Django Software Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.djangoproject.com/foundation/">https://www.djangoproject.com/foundation/</a>
    * Development of Django is supported by an independent foundation established as a 501(c)(3) non-profit. Like most open-source foundations, the goal of the Django Foundation is to promote, support, and advance its open-source project: in our case, the Django Web framework.

* Dronecode Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.dronecode.org">https://www.dronecode.org</a>
    * The Dronecode Foundation fosters communities and innovation through open-standards using open-source. Dronecode is a vendor-neutral foundation for open source drone projects. We are a US-based non-profit under the Linux Foundation and provide open source governance, infrastructure, and services to software & hardware projects. We work with developers, end-users, and adopting vendors from around the world.
 
* Drupal Association
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.drupal.org/association">https://www.drupal.org/association</a>
    * The Drupal Association is the non-profit organization dedicated to accelerating the Drupal software project, fostering the community, and supporting its growth.

 <a name="e"></a>
* Eclipse Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.eclipse.org/org/">https://www.eclipse.org/org/</a>
    * The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation. The Foundation is home to the Eclipse IDE, Jakarta EE, and over 350 open source projects, including runtimes, tools, and frameworks for a wide range of technology domains such as the Internet of Things, automotive, geospatial, systems engineering, and many others.
 
* Electronic Frontier Foundation
    * Type: Independent
    * Legal status: United States 501(c)(3)
    * <a href="https://www.eff.org/">https://www.eff.org/</a>
    * The Electronic Frontier Foundation is the leading nonprofit organization defending civil liberties in the digital world. Founded in 1990, EFF champions user privacy, free expression, and innovation through impact litigation, policy analysis, grassroots activism, and technology development. We work to ensure that rights and freedoms are enhanced and protected as our use of technology grows.

* Enabling Linux In Safety Applications (ELISA)
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://elisa.tech">https://elisa.tech</a>
    * The mission of the Enabling Linux In Safety Applications (ELISA) project is to make it easier for companies to build and certify Linux-based safety-critical applications – systems whose failure could result in loss of human life, significant property damage or environmental damage. ELISA members are working together to define and maintain a common set of tools and processes that can help companies demonstrate that a specific Linux-based system meets the necessary safety requirements for certification.

* Endless OS Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(4)
    * <a href="https://www.endlessos.org">https://www.endlessos.org</a>
    * Our mission is to help all people and communities connect with technology. We believe that access to personal computing is critical for productivity, learning and job skills. We have dedicated the last 8 years to designing and delivering an operating system and tools that give people access to, and control over their technology. With our tools for productivity, creativity, and learning through play, we help people of all backgrounds engage in the digital economy on more meaningful terms.

<a name="f"></a>
* F# Software Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://foundation.fsharp.org/">https://foundation.fsharp.org/</a>
    * The mission of the F# Software Foundation is to promote, protect, and advance the F# programming language, and to support and facilitate the growth of a diverse and international community of F# programmers.

* Federated AI Ecosystem
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.fedai.org">https://www.fedai.org</a> 
    * FedAI ecosystem enables all partners fully exploiting their data values, and safely promoting applications in their vertical industry to achieve more goals. Through the open-source project, we assist enterprises and institutions in AI empowerment and enhance abilities of self-modeling technologies.

* Fintech Open Source Foundation (FINOS)
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.finos.org">https://www.finos.org</a>
    * The Fintech Open Source Foundation (FINOS) is an independent 501(c)(6) nonprofit organization whose purpose is to accelerate collaboration and innovation in financial services through the adoption of open source software, standards and best practices.

* Foundation for Public Code
    * Type: Independent
    * Legal/financial status: Dutch Vereniging
    * <a href="https://publiccode.net">https://publiccode.net</a>
    * We help public organizations collectively develop and maintain public code. This results in higher quality services for the public that are more cost effective, with less risk and more local control. We define ‘public code’ as open source software developed by public organizations, together with the policy and guidance needed for reuse.

* Free and Open Source Silicon Foundation
    * Type: Independent
    * Legal/financial status: United Kingdom not-for-profit limited by guarantee
    * <a href="https://www.fossi-foundation.org">https://www.fossi-foundation.org</a>
    * FOSSi Foundation is a non-profit foundation with the mission to promote and assist free and open digital hardware designs and their related ecosystems. FOSSi Foundation operates as an open, inclusive, vendor-independent group.
 
* Free Knowledge Institute
    * Type: Independent
    * Legal/financial status: Dutch Stichting
    * <a href="http://freeknowledge.eu/">http://freeknowledge.eu/</a>
    * The Free Knowledge Institute (FKI) is a non-profit organisation that fosters equal access to tools for production and exchange of knowledge in all areas of society. Inspired by the Free Software movement, the FKI promotes freedom of use, modification, copying and distribution of knowledge in several different but closely related fields. Accordingly, it promotes the commons economy.

* Free Silicon Foundation
    * Type: Independent
    * Legal/financial status: unknown type of NGO
    * <a href="https://f-si.org">https://f-si.org</a>
    * The Free Silicon Foundation (F-Si) is a nonprofit organization with the scope of promoting: 1. Free and Open Source (FOS) CAD tools for designing integrated circuits, 2. the sharing of hardware designs and libraries, 3. common standards, 4. the freedom of users in the context of silicon integrated circuits.

* Free Software Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.fsf.org/">https://www.fsf.org/</a>
    * The Free Software Foundation is working to secure freedom for computer users by promoting the development and use of free (as in freedom) software and documentation—particularly the GNU operating system—and by campaigning against threats to computer user freedom like Digital Restrictions Management (DRM) and software patents.

* Free Software Foundation Europe
    * Type: Independent
    * Legal/financial status: German e.V.
    * <a href="https://fsfe.org">https://fsfe.org</a>
    * Free Software Foundation Europe is a charity that empowers users to control technology. Software is deeply involved in all aspects of our lives; and it is important that this technology empowers rather than restricts us. Free Software gives everybody the rights to use, understand, adapt and share software. These rights help support other fundamental freedoms like freedom of speech, press and privacy.

* Free Software Foundation India
    * Type: Independent
    * Legal/financial status: Indian Nonprofit
    * <a href="https://www.fsf.org.in/">https://www.fsf.org.in/</a>
    * FSF India is a non-profit organisation committed to advocating, promoting and propagating the use and development of swatantra software in India.

* Free Software Foundation Latin America
    * Type: Independent
    * Legal/financial status: Unknown
    * <a href="https://www.fsfla.org/">https://www.fsfla.org/</a>
    * The Free Software Foundation Latin America (FSFLA) is an organization formed by people who strongly believe in Free Software, who got together in order to promote and defend the use and development of Free Software, and people's rights to use, study, copy, modify and redistribute software. The Constitution of FSF Latin America details our mission and objectives.

* FreeBSD Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://freebsdfoundation.org/">https://freebsdfoundation.org/</a>
    * The FreeBSD Foundation is a 501(c)(3), US based, non-profit organization dedicated to supporting and promoting the FreeBSD Project and community worldwide. Funding comes from individual and corporate donations and is used to fund and manage projects, fund conferences and developer summits, and provide travel grants to FreeBSD developers.

* Fundación Vía Libre
    * Type: Independent
    * Legal/financial status: Argentinian NGO
    * <a href="https://www.vialibre.org.ar/">https://www.vialibre.org.ar/</a>
    * Fundación Vía Libre is a non-profit civil organization born in Córdoba, Argentina, in 2000. Initially focused on Free Software public policies, dissemination of knowledge and sustainable development, the Foundation reoriented its mission to broader issues such as the impact and influence of digital technologies on Human Rights, with special attention to social, economic, and cultural rights and civil liberties.

<a name="g"></a>
* Gentoo Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.gentoo.org/inside-gentoo/foundation/">https://www.gentoo.org/inside-gentoo/foundation/</a>
    * In order to sustain the current quality and development swiftness the Gentoo project needs a framework for intellectual property protection and financial contributions while limiting the contributors’ legal exposure. The Gentoo Foundation will embody this framework without intervening in the Gentoo development. This latter aspect should be seen as a clear separation between coordinating the Gentoo development and protecting Gentoo’s assets. Both are distinct concepts requiring different skills and working methods.

* GNOME Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://foundation.gnome.org/">https://foundation.gnome.org/</a>
    * The GNOME Foundation is a non-profit organization that furthers the goals of the GNOME Project, helping it to create a free software computing platform for the general public that is designed to be elegant, efficient, and easy to use.

* GraphQL Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://foundation.graphql.org">https://foundation.graphql.org</a>
    * The GraphQL Foundation is a neutral foundation founded by global technology and application development companies. The GraphQL Foundation encourages contributions, stewardship, and a shared investment from a broad group in vendor-neutral events, documentation, tools, and support for GraphQL.

<a name="h"></a>
* Haiku, Inc.
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.haiku-inc.org/">https://www.haiku-inc.org</a>
    * Haiku, Inc. has supported Haiku's development and infrastructure since 2003.

* Haskell.org
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.haskell.org/haskell-org-committee/">https://www.haskell.org/haskell-org-committee/</a>
    * Haskell.org is a non-profit organization that oversees the Haskell.org website and a number of other services for the Haskell community.

* Haskell Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://haskell.foundation/en/">https://haskell.foundation/en/</a>
    * The Haskell Foundation (HF) is an independent, non-profit organization dedicated to broadening the adoption of Haskell, by supporting its ecosystem of tools, libraries, education, and research.

* Hyperledger
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.hyperledger.org">https://www.hyperledger.org</a>
    * Hyperledger is an open source community focused on developing a suite of stable frameworks, tools and libraries for enterprise-grade blockchain deployments.
It serves as a neutral home for various distributed ledger frameworks including Hyperledger Fabric, Sawtooth, Indy, as well as tools like Hyperledger Caliper and libraries like Hyperledger Ursa.

<a name="i"></a>
* Identity Commons
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.idcommons.org">https://www.idcommons.org</a>
    * The purpose of Identity Commons is to support, facilitate, and promote the creation of an open identity layer for the Internet, one that maximizes control, convenience, and privacy for the individual while encouraging the development of healthy, interoperable communities.

* InnerSourceCommons.org
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://innersourcecommons.org">http://innersourcecommons.org</a>
    * The InnerSource Commons (ISC) is a growing community of practitioners with the goal of creating and sharing knowledge about InnerSource: the use of open source best practices for software development within the confines of an organization. Founded in 2015, the InnerSource Commons is now supporting and connecting over seventy companies, academic institutions, and government agencies.

* Interledger Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://interledger.org">https://interledger.org</a>
    * The Interledger Foundation and its community are building pathways to financial access and opportunity across the world, connecting humanity in a new way.

* Internet Systems Consortium, Inc.
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://www.isc.org/">http://www.isc.org/</a>
    * Founded in 1994, ISC develops and distributes three open source Internet networking software packages: BIND 9, ISC DHCP, and Kea DHCP. BIND 9, ISC’s Domain Name System (DNS) software program, is widely used on the Internet by enterprises and service providers, offering a robust and stable platform on top of which organizations can build distributed computing systems. ISC DHCP implements the Dynamic Host Configuration Protocol for connection to an IP network, offering a complete solution for implementing DHCP servers, relay agents, and clients. ISC DHCP is a mature program with many features, but it can be cumbersome for operators to maintain. Kea DHCP is ISC newer DHCP software, and is designed for modular extension, dynamic reconfiguration, and high performance. 

* ITPUG (Italian PostgreSQL Users' Group)
    * Type: Independent
    * Legal/financial status: Italian Non-Profit Association
    * <a href="https://www.itpug.org">https://www.itpug.org</a> 
    * ITPUG - Italian PostgreSQL Users Group is a non-profit association for the promotion, dissemination and protection of Free Software (FLOSS) in Italy. In particular, the ITPUG association promotes the adoption of PostgreSQL, one of the most successful open source projects.

<a name="j"></a>
* Joint Development Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.jointdevelopment.org">https://www.jointdevelopment.org</a>
    * The Joint Development Foundation provides the corporate and legal infrastructure to enable groups to quickly establish and operate lightweight collaborations to develop technical specifications, standards, and source code.  Joint Development Foundation Projects are ideal for specification development projects or as a place to do incubation projects before taking those projects to a larger standards organization.

* JPUG (Japanese PostgreSQL Users' Group)
    * Type: Independent
    * Legal/financial status: Japanese Non-Profit Corporation
    * <a href="https://www.postgresql.jp">https://www.postgresql.jp</a>
    * The largest of several foundations supporting PostgreSQL. JPUG exists to promote development of, and training in, the PostgreSQL database system as well as funding several conferences throughout the year. They also certify PostgreSQL DBAs.

<a name="k"></a>
* KDE e.V.
    * Type: Independent
    * Legal/financial status: German e.V.
    * <a href="https://ev.kde.org/">https://ev.kde.org/</a>
    * KDE e.V. is a registered non-profit organization that represents the KDE Project in legal and financial matters. The Association aids in creating and distributing KDE by securing cash, hardware, and other donations, then using donations to aid KDE development and promotion.

* Kotlin Foundation
    * Type: Independent
    * Legal/financial status: Delaware, USA nonprofit
    * <a href="https://kotlinlang.org/docs/kotlin-foundation.html">https://kotlinlang.org/docs/kotlin-foundation.html</a>
    * The Kotlin Foundation was created by JetBrains and Google with the mission to protect, promote and advance the development of the Kotlin programming language. The Foundation secures Kotlin's development and distribution as Free Software, meaning that it is able to be freely copied, modified and redistributed, including modifications to the official versions.

* Krita Foundation
    * Type: Independent
    * Legal/financial status: Dutch Stichting
    * <a href="https://krita.org/en/about/krita-foundation/">https://krita.org/en/about/krita-foundation/</a>
    * The Krita Foundation (2012) is an independent public benefit organization with the purpose of improving and making available as widely as possible the digital painting application Krita.

* Kuali Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://kuali.org/">https://kuali.org/</a>
    * The Kuali Foundation is a non-profit organization dedicated to the development of open source administrative software solutions for higher education.

<a name="l"></a>
* LF AI & Data Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://lfaidata.foundation">https://lfaidata.foundation</a>
    * The LF AI & Data Foundation supports open source projects within artificial intelligence, machine learning, deep learning and the data space. You can think of us as a greenhouse growing and sustaining open source AI, ML, DL and Data projects from seed to fruition. The LF AI & Data Foundation provides the support to projects for open development to occur among a diverse and thriving community, in addition to a number of enabling services that include membership and funding management, ecosystem development, legal support, PR/marketing/communication, events support, and compliance scans.

* LF Edge
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.lfedge.org">https://www.lfedge.org</a>
    * LF Edge is an umbrella organization that aims to establish an open, interoperable framework for edge computing independent of hardware, silicon, cloud, or operating system. By bringing together industry leaders, LF Edge will create a common framework for hardware and software standards and best practices critical to sustaining current and future generations of IoT and edge devices. We are fostering collaboration and innovation across the multiple industries including industrial manufacturing, cities and government, energy, transportation, retail, home and building automation, automotive, logistics and health care — all of which stand to be transformed by edge computing.

* LF Energy
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.lfenergy.org">https://www.lfenergy.org</a>
    * LF Energy is an open source foundation focused on the power systems sector, hosted within The Linux Foundation. LF Energy provides a neutral, collaborative community to build the shared digital investments that will transform the world’s relationship to energy.

* LF Networking
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.lfnetworking.org">https://www.lfnetworking.org</a>
    * Network transformation creates an opportunity for the industry to take advantage of open source, open technology, choice, and cloud economics. LFN software and projects provide platforms and building blocks for Network Infrastructure and Services across Service Providers, Cloud Providers, Enterprises, Vendors, and System Integrators that enable rapid interoperability, deployment, and adoption. LF Networking supports the largest set of networking projects with the broadest community in the industry that collaborate on this opportunity.

* Linux Foundation
    * Type: Umbrella
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://linuxfoundation.org/">https://linuxfoundation.org/</a>
    * The Linux Foundation is dedicated to building sustainable ecosystems around open source projects to accelerate technology development and industry adoption. Founded in 2000, the Linux Foundation provides unparalleled support for open source communities through financial and intellectual resources, infrastructure, services, events, and training. Working together, the Linux Foundation and its projects form the most ambitious and successful investment in the creation of shared technology.

* Linux Professional Institute
    * Type: Independent
    * Legal/financial status: Canadian Nonprofit
    * <a href="https://www.lpi.org/about-lpi/our-purpose">https://www.lpi.org/about-lpi/our-purpose</a>
    * Linux Professional Institute (LPI) is the global certification standard and career support organization for open source professionals. With more than 200,000 certification holders, it's the world’s first and largest vendor-neutral Linux and open source certification body. LPI has certified professionals in over 180 countries, delivers exams in multiple languages, and has hundreds of training partners. Our mission is to promote the use of open source by supporting the people who work with it.

* LLVM Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://foundation.llvm.org/">https://foundation.llvm.org</a>
    * The LLVM Foundation is a nonprofit whose mission is to support education and advancement of the field of compilers and tools through educational events, grants and scholarships, and increasing diversity with the field of compilers, tools, and the LLVM project.

<a name="m"></a>
* Mapzen
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.mapzen.com/about/">https://www.mapzen.com/about/</a>
    * The Mapzen projects at Linux Foundation form an open and accessible mapping platform. We're focused on the core components of geo platforms, including search, rendering, navigation, and data. We continue to take a radical approach to working on these components—we build them all entirely in the open.

* Mobile Native Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://mobilenativefoundation.org">https://mobilenativefoundation.org</a>
    * The Mobile Native Foundation provides a place to collaborate on open source projects and discuss wide ranging topics in order to improve processes and technologies for large-scale Android and iOS applications.

* Mozilla Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://foundation.mozilla.org/en/">https://foundation.mozilla.org/en/</a>
    * Mozilla invests in bold ideas, global leaders, and the convening and campaign power of people. For more than two decades, we’ve worked across borders, disciplines, and technologies to fuel a movement to realize the full potential of the internet.

<a name="n"></a>
* NetBSD Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.netbsd.org/foundation/">https://www.netbsd.org/foundation/</a>
    * The NetBSD Foundation serves as the legal entity which owns several of the NetBSD Project servers, handles donations of money, services, hardware or time to the project, and administers NetBSD copyrights.

* NLnet Foundation
    * Type: Independent
    * Legal/financial status: Netherlands ANBI
    * <a href="https://nlnet.nl">https://nlnet.nl</a>
    * The foundation "Stichting NLnet" stimulates network research and development in the domain of Internet technology. The articles of association for the NLnet foundation state: "to promote the exchange of electronic information and all that is related or beneficial to that purpose".

* NLnet Labs Foundation
    * Type: Independent
    * Legal/financial status: Netherlands ANBI
    * <a href="https://www.nlnetlabs.nl/organisation/">https://www.nlnetlabs.nl/organisation/</a>
    * Stichting NLnet Labs (NLnet Labs for short) is a not-for-profit (ANBI, Algemeen Nut Beogende Instelling) founded in 1999 in the Netherlands. The objectives according to the bylaws are: to develop Open Source software and open standards for the benefit of the Internet. The mission is to provide globally recognised innovations and expertise for those technologies that turn a network of networks into an Open Internet for All.

* NumFocus, Inc.
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://numfocus.org">https://numfocus.org</a>
    * The mission of NumFOCUS is to promote open practices in research, data, and scientific computing by serving as a fiscal sponsor for open source projects and organizing community-driven educational programs.

<a name="o"></a>
* OASIS Open
    * Type: Umbrella
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.oasis-open.org/org/">https://www.oasis-open.org/org/</a>
    * One of the most respected, non-profit standards bodies in the world, OASIS Open offers projects—including open source projects—a path to standardization and de jure approval for reference in international policy and procurement.

* OASIS Open Europe Foundation
    * Type: Affiliate of OASIS Open
    * Legal/financial status: Dutch Stichting
    * <a href="https://www.oasis-open.eu">https://www.oasis-open.eu</a>
    * As the European sovereign affiliate organisation to the international nonprofit, OASIS Open, we work to advance and support Europe’s role in open source and open standards development.

* ODPi
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.odpi.org/about">https://www.odpi.org/about</a>
    * ODPi is a member-driven organization made up of the leaders in big data governance, working together to create and standardize solutions that work across platforms, systems and products. All members have the opportunity to influence decisions and create industry best practices, specifications and test suites. Whether you’re a product vendor who needs a standard for data sharing, a data professional working to share data between products and platforms across the enterprise, or a practitioner working to get a variety of data products to work seamlessly together, ODPi can help. ODPi makes it easy to replicate, share, use, and protect data across the whole enterprise.

* One Laptop Per Child Association, Inc.
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.onelaptopperchild.org">https://www.onelaptopperchild.org</a>
    * OLPC exists to use quality education to combat the issues that burden the youngest and most hopeful people: our children. The program began at the Massachusetts Institute of Technology (MIT) in 2005. Since its inception, OLPC has provided more than 3 million educational laptops to children around the world. 

* Open Accessibility
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://wiki.linuxfoundation.org/accessibility/start">https://wiki.linuxfoundation.org/accessibility/start</a>
    * First chartered in 2004 as the Linux Foundation Accessibility Workgroup (LFA), the Open Accessibility (A11y) Group functions today within the Linux Foundation to establish free and open standards that enable comprehensive universal access to various computing platforms, applications, and services. Open A11y makes it easier for developers, ISVs, and distributions to support assistive technologies (AT). Assistive technologies enable individuals to make full use of computer-based technology despite variability in physical or sensory abilities due to illness, aging or disability.

* Open Alliance for Cloud Adoption
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.oaca-project.org">https://www.oaca-project.org</a>
    * The Open Alliance for Cloud Adoption is a member-led consortium of global technology organizations dedicated to easing the adoption of interoperable solutions and services addressing cloud computing for enterprises across the globe.

* Open API Initiative
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.openapis.org">https://www.openapis.org</a>
    * The OpenAPI Initiative (OAI) was created by a consortium of forward-looking industry experts who recognize the immense value of standardizing on how APIs are described. As an open governance structure under the Linux Foundation, the OAI is focused on creating, evolving and promoting a vendor neutral description format. The OpenAPI Specification was originally based on the Swagger Specification, donated by SmartBear Software.

* Open Bioinformatics Foundation
    * Type: Fiscal sponsored (SPI)
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://open-bio.org/">https://open-bio.org/</a>
    * The Open Bioinformatics Foundation is a non-profit, volunteer-run group that promotes open source software development and Open Science within the biological research community.

* Open Collective Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://opencollective.foundation">https://opencollective.foundation</a>
    * Offering fiscal sponsorship as a service, to help US-based initiatives with a charitable purpose get up and running in one day. Our online FOSS platform enables you to easily receive, manage, and disburse funding. Reporting is automatic and in real time. We have partnerships with major institutional funders and companies in the tech area, facilitating grants and coalitions in addition to enabling crowdfunding. Donations are tax deductible.

* Open Compute Project
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.opencompute.org">https://www.opencompute.org</a>
    * The Open Compute Project (OCP) is a collaborative community focused on redesigning hardware technology to efficiently support the growing demands on compute infrastructure. 

* Open Container Initiative
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://opencontainers.org">https://opencontainers.org</a>
    * The Open Container Initiative is an open governance structure for the express purpose of creating open industry standards around container formats and runtimes. Established in June 2015 by Docker and other leaders in the container industry, the OCI currently contains two specifications: the Runtime Specification (runtime-spec) and the Image Specification (image-spec). The Runtime Specification outlines how to run a “filesystem bundle” that is unpacked on disk. At a high-level an OCI implementation would download an OCI Image then unpack that image into an OCI Runtime filesystem bundle. At this point the OCI Runtime Bundle would be run by an OCI Runtime.

* Open Cybersecurity Alliance
    * Type: Sub-organisation of OASIS Open Projects
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://opencybersecurityalliance.org">https://opencybersecurityalliance.org</a>
    * OCA is building an open ecosystem where cybersecurity products interoperate without the need for customized integrations. Using community-developed standards and practices, we’re simplifying integration across the threat lifecycle.

* Open Distributed Infrastructure Management (ODIM)
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://odim.io">https://odim.io</a>
    * The ODIM community is a bold collaborative open source initiative to bring together a critical mass of infrastructure management and orchestration stakeholders to define and execute the collaborative work in several areas.

* OpenDoc Society
    * Type: Independent
    * Legal/financial status: Dutch Stichting
    * <a href="http://www.opendocsociety.org/">http://www.opendocsociety.org/</a>
    * OpenDoc Society brings together individuals and organisations with a stake or interest in the openness and future of documents to learn from each other and share knowledge - about core technologies, available tools, policy issues, transition strategies, legal aspects and of course the latest innovations. Whether you are a developer, publisher, decision maker, educator, vendor, IT manager, academic, writer, archivist or just an involved citizen - OpenDocSociety.org brings you together with interesting like-minded people to learn from and cooperate with.

* Open Information Security Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://suricata-ids.org">https://suricata-ids.org</a>
    * The Open Information Security Foundation is in charge of developing the Suricata Intrusion Detection and Prevention Software (IDS/IPS) and does so through in an open source and transparent way. Suricata is released under GPL v2 and commercial licenses. The OISF does provide online and onsite training on Suricata as well.

* Open Infrastructure Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://openinfra.dev">https://openinfra.dev</a>
    * When the OpenInfra Foundation formed to govern the OpenStack project in 2012, clouds mainly offered virtual machines in a datacenter. The Foundation evolved as datacenter composition evolved into a mix of bare metal, VMs, and containers. While the OpenStack project is seen as the de facto open source platform for operating cloud infrastructure around the world, the Foundation realized more technology is needed to meet all of the diverse use cases, and we want to make sure it’s developed in the open, using the same proven approach to open source. For this reason, we expanded our focus as a foundation and are helping to establish new open source communities to advance areas where technology can successfully contribute to the development of open infrastructure: AI/Machine Learning, CI/CD, Container Infrastructure, Edge Computing and of course, Public, Private and Hybrid Clouds.

* Open Mainframe Project
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.openmainframeproject.org">https://www.openmainframeproject.org</a>
    * The Open Mainframe Project is intended to serve as a focal point for deployment and use of Linux and Open Source in a mainframe computing environment. The Project intends to increase collaboration across the mainframe community and to develop shared tool sets and resources. Furthermore, the Project seeks to involve the participation of academic institutions to assist in teaching and educating the mainframe engineers and developers of tomorrow.

* Open Manufacturing Platform (OMP)
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://open-manufacturing.org">https://open-manufacturing.org</a>
    * The Open Manufacturing Platform (OMP) brings together business leaders and technologists from manufacturing companies, technology solution providers, systems integrators and more to drive innovation across the manufacturing community and value chain. We bring forward platform-agnostic solutions, open standards and technologies to enable smart manufacturing, break down data silos, and solve real problems for all of us—regardless of technology, solution provider, or cloud platform.

* Open Mobility Foundation
    * Type: Sub-organisation of OASIS Open
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.openmobilityfoundation.org">https://www.openmobilityfoundation.org</a>
    * The Open Mobility Foundation (OMF) is an open-source software foundation that creates a governance structure around open-source mobility tools, beginning with a focus on the Mobility Data Specification (MDS). By creating an open source foundation, OMF is able to offer a safe, efficient environment for stakeholders including municipalities, companies, technical, privacy, and policy experts, and the public to shape urban mobility management tools that help public agencies accomplish their mobility policy goals.

* Open Networking Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://opennetworking.org">https://opennetworking.org</a>
    * The Open Networking Foundation (ONF) is a non-profit operator led consortium driving transformation of network infrastructure and carrier business models.

* Open Plans
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.openplans.org">https://www.openplans.org</a>
    * OpenPlans is a non-profit technology organization helping to open up government and improve transportation systems. We build open source software. We help agencies open up their data. We report on urban issues. We offer technical assistance to public agencies, and we build communities around our initiatives in order to seed an open and evolving ecosystem of technology tools that further the public interest.

* Open Scalable File Systems (OpenSFS)
    * Type: Independent
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.opensfs.org/">https://www.opensfs.org/</a>
    * OpenSFS is a non-profit industry organization that supports vendor-neutral development and promotion of Lustre, an open-source file system that supports many of the world's largest and most complex computing environments.

* Open Search Foundation
    * Type: Independent
    * Legal/financial status: German e.V.
    * <a href="https://opensearchfoundation.org/en/open-search-foundation-home/">https://opensearchfoundation.org/en/open-search-foundation-home/</a>
    * The Open Search Foundation e.V. is a movement of people and organisations that work together to create the foundations for independent, free and self-determined access to information on the Internet. In collaboration with research institutions, computer centres and other partners, we’re committed to searching the web in a way that benefits everyone. The promotion of research in the field of search engines, plus education and collaboration, form the pillars of our work.

* Open Source Applications Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://www.osafoundation.org/">http://www.osafoundation.org/</a>
    * OSAF is a non-profit organization working on Chandler Project, a Note-to-Self Organizer designed for personal and small-group task management and calendaring. Chandler consists of a cross-platform desktop application (Windows, Mac OS X, Linux), the Chandler Hub Sharing Service and Chandler Server

* Open Source Collective
    * Type: Independent
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://oscollective.org">https://oscollective.org</a>
    * For a sustainable and healthy open source ecosystem. Are you part of an open source project looking to raise funds transparently, or a company looking to support FOSS, with minimal hassle? Join us! We are a non-profit working for the common interests of those who create and use free and open source software, already enabling funding for thousands of projects through a FOSS online platform that automates admin and provides transparency. We can accept any open source project, in any language, anywhere in the world, as well as open source related meetup groups and conferences, and advocacy, research, and awareness initiatives.

* Open Source Elections Technology Institute (OSET)
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.osetfoundation.org/">https://www.osetfoundation.org/</a>
    * The OSET Institute is about researching, developing, and making innovative public election technology (i.e., publicly available open source technology subject to an OSI-accredited license for production uses, and AGPL3 for all other research and some production applications) in order to increase verification, accuracy, security, and transparency (in process), and ensure that ballots are counted as cast. This public benefit work is focused on increasing integrity in elections, while lowering costs, and improving usability.  If the work is effective, it should ease participation and make the use of the technology easy, convenient, even delightful for administrators and voters alike.

* Open Source Geospatial Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.osgeo.org">https://www.osgeo.org</a>
    * The Open Source Geospatial Foundation (OSGeo) is a not-for-profit organization whose mission is to foster global adoption of open geospatial technology by being an inclusive software foundation devoted to an open philosophy and participatory community driven development.

* Open Source Hardware Association
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.oshwa.org">https://www.oshwa.org</a>
    * The Open Source Hardware Association (OSHWA) aims to foster technological knowledge and encourage research that is accessible, collaborative and respects user freedom. OSHWA’s primary activities include hosting the annual Open Hardware Summit and maintaining the Open Source Hardware certification, which allows the community to quickly identify and represent hardware that complies with the community definition of open source hardware.

* Open Source Initiative
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://opensource.org/">http://opensource.org/</a>
    * The Open Source Initiative (OSI) is a non-profit corporation formed to educate about and advocate for the benefits of open source and to build bridges among different constituencies in the open-source community.

* Open Source Matters
    * Type: Independent
    * Legal/financial status: United States corporation
    * <a href="https://www.opensourcematters.org/">https://www.opensourcematters.org</a>
    * Open Source Matters (OSM) is a not-for-profit organisation created to serve the financial and legal interests of the Joomla! project.

* Open Source Security Foundation (OpenSSF)
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://openssf.org">https://openssf.org</a>
    * The OpenSSF is a cross-industry collaboration that brings together leaders to improve the security of open source software (OSS) by building a broader community, targeted initiatives, and best practices
The OpenSSF brings together open source security initiatives under one foundation to accelerate work through cross-industry support. This is beginning with the Core Infrastructure Initiative and the Open Source Security Coalition, and will include new working groups that address vulnerability disclosures, security tooling and more.

* Open Treatments Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.opentreatments.org">https://www.opentreatments.org</a>
    * We are non-profit organization with a mission is to enable treatments for all genetic diseases regardless of rarity or geography using the OpenTreatments software platform. We will decentralize drug development and empower patients, families, or motivated individuals to create a treatment for a disease impacting their loved one. We will enable the hand off of these therapies to commercial/governmental/philanthropic entities to ensure patients around the world get access to the therapies for the years to come.

* OpenBSD Foundation
    * Type: Independent
    * Legal/financial status: Canadian Nonprofit
    * <a href="https://www.openbsdfoundation.org">https://www.openbsdfoundation.org</a>
    * Formally, the corporation's objects are to support and further the development, advancement, and maintenance of free software based on the OpenBSD operating system, including the operating system itself and related free software projects.

* OpenID Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://openid.net/foundation/">https://openid.net/foundation/</a>
    * The OpenID Foundation (OIDF) promotes, protects and nurtures the OpenID community and technologies. The OpenID Foundation is a non-profit international standardization organization of individuals and companies committed to enabling, promoting and protecting OpenID technologies.

* OpenJS Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://openjsf.org">https://openjsf.org</a>
    * Developers rely on a growing portfolio of open source technologies to create, test and deploy critical applications. By creating a center of gravity for the open source JavaScript ecosystem, the OpenJS Foundation’s mission is to drive broad adoption and ongoing development of key JavaScript solutions and related technologies.

* OpenStreetMap Foundation
    * Type: Independent
    * Legal/financial status: United Kingdom not-for-profit limited by guarantee
    * <a href="https://wiki.openstreetmap.org/wiki/Foundation">https://wiki.openstreetmap.org/wiki/Foundation</a>
    * The OpenStreetMap Foundation is a not-for-profit organization that supports the OpenStreetMap Project. The OpenStreetMap Project based at OpenStreetMap.org, is the worldwide mapping effort that includes over two million volunteers around the globe.

* OpenUK
    * Type: Independent
    * Legal/financial status: United Kingdom not-for-profit limited by guarantee
    * <a href="https://openuk.uk">https://openuk.uk</a>
    * We are the UK not for profit organisation committed to develop and sustain UK leadership in Open Technology, being the 3 Opens open source software, open source hardware and open data, across the UK.

* O-RAN Software Community
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://o-ran-sc.org">https://o-ran-sc.org</a>
    * The O-RAN Software Community (SC) is a collaboration between the O-RAN Alliance and Linux Foundation with the mission to support the creation of software for the Radio Access Network (RAN). The RAN is the next challenge for the open source community. The O-RAN SC plans to leverage other LF network projects, while addressing the challenges in performance, scale, and 3GPP alignment.

* Oregon State University Open Source Lab Alliance
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://osuosl.org/about">https://osuosl.org/about</a>
    * The Open Source Lab is a nonprofit organization working for the advancement of open source technologies. The lab, in partnership with the School of Electrical Engineering and Computer Science at Oregon State University, provides hosting for more than 160 projects, including those of worldwide leaders like the Apache Software Foundation, the Linux Foundation and Drupal. Together, the OSL’s hosted sites deliver nearly 430 terabytes of information to people around the world every month. The most active organization of its kind, the OSL offers world-class hosting services, professional software development and on-the-ground training for promising students interested in open source management and programming.

* OW2
    * Type: Independent
    * Legal/financial status: French 1901 Non-profit Association
    * <a href="https://www.ow2.org">https://www.ow2.org</a>
    * The mission of OW2 is to a) promote the development of open-source middleware, generic business applications, cloud computing platforms and b) foster a vibrant community and business ecosystem.

<a name="p"></a>
* Participatory Culture Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://pculture.org">https://pculture.org</a>
    * We develop technology and services that ensure everyone has access to all that the internet has to offer. From the abundance of free education available, to gaining insights about the many different cultures around the globe, information is critical to building a more equitable and peaceful society. At PCF, we work together with a single-minded focus. Driven by passion not profit, our team is committed to building technology and services that make a difference.

* Plan 9 Foundation
    * Type: Independent
    * Legal/financial status: Oregon nonprofit
    * <a href="https://plan9foundation.org/index.html">https://plan9foundation.org/index.html</a>
    * The Plan 9 Foundation, an Oregon nonprofit corporation, maintains the archives of the Plan 9 operating system software and supports future development by following the practice and processes set forth by the original authors. We strive to continue the work started at Bell Labs in the 1980s. The approach used to model and build distributed systems at that time continues to influence systems to this day.

* Plone Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://plone.org/foundation">https://plone.org/foundation</a>
    * We provide support for development and marketing, and are modeled after similar ventures such as the Apache Software Foundation. We are the legal owner of the Plone codebase, trademarks, and domain names. Our goal is to ensure Plone remains the premier open source content management system to broaden its acceptance and visibility.

* PostgreSQL Community Association of Canada (PGCAC)
    * Type: Independent
    * Legal/financial status: Canadian Nonprofit
    * <a href="https://www.postgres.ca/">https://www.postgres.ca</a>
    * PostgreSQL Community Association of Canada is a Canadian not-for-profit corporation that holds assets for the PostgreSQL project such as domain names and trademarks.

* PostgreSQL Europe
    * Type: Independent
    * Legal/financial status: French 1901 Non-profit Association
    * <a href="https://postgresql.eu">https://postgresql.eu</a>
    * Welcome to PostgreSQL Europe, the "umbrella group" for PostgreSQL User Groups in Europe and for users in regions that don't have a local user group. Our goal is to promote the use of PostgreSQL in Europe. We do this primarily by assisting local user groups to promote the product locally through conferences and other advocacy, as well as running European-wide conferences and other activities.

* PostgreSQLFr.org
    * Type: Independent
    * Legal/financial status: French 1901 Non-profit Association
    * <a href="https://www.postgresql.fr//asso:accueil">https://www.postgresql.fr//asso:accueil</a>
    * PostgreSQLFr est une association loi 1901 qui a pour but de promouvoir le système de gestion de bases de données PostgreSQL dans les pays francophones. Les actions menées sont multiples : organisation de conférences, comme lors du PGDay, aide aux utilisateurs (par le forum, etc.), traduction de la doc, Groupe de travail Inter-Entreprises, etc. L'association a été créée en 2004 et compte actuellement plusieurs dizaines d'adhérents. Elle fonctionne grâce à ses bénévoles et aux adhésions de ces membres.

* PostgreSQL.US
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://postgresql.us">https://postgresql.us</a>
    * The United States PostgreSQL Association, affectionately known as PgUS, is a IRS 501(c)(3) public charity. Our purpose is to support the growth and education of PostgreSQL, The World's Most Advanced Open Source Database.

* Python Software Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.python.org/psf/">https://www.python.org/psf/</a>
    * The Python Software Foundation (PSF) is a 501(c)(3) non-profit corporation that holds the intellectual property rights behind the Python programming language. We manage the open source licensing for Python version 2.1 and later and own and protect the trademarks associated with Python. We also run the North American PyCon conference annually, support other Python conferences around the world, and fund Python related development with our grants program and by funding special projects.

<a name="r"></a>
* R Consortium
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.r-consortium.org">https://www.r-consortium.org</a>
    * The central mission of the R Consortium is to work with and provide support to the R Foundation and to the key organizations developing, maintaining, distributing and using R software through the identification, development and implementation of infrastructure projects.

* R Foundation
    * Type: Fiscal sponsored (Free Software Foundation)
    * Legal/financial status: Austrian non-profit association (Verein)
    * <a href="https://www.r-project.org/foundation/">https://www.r-project.org/foundation/</a>
    * The R Foundation is a not for profit organization working in the public interest. It has been founded by the members of the R Development Core Team in order to (1) Provide support for the R project and other innovations in statistical computing. We believe that R has become a mature and valuable tool and we would like to ensure its continued development and the development of future innovations in software for statistical and computational research. (2) Provide a reference point for individuals, institutions or commercial enterprises that want to support or interact with the R development community. (3) Hold and administer the copyright of R software and documentation.

* RISC-V International
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://riscv.org/about/">https://riscv.org/about/</a>
    * RISC-V International comprises more than 500 members building the first open, collaborative community of software and hardware innovators powering innovation at the edge forward. Through various events and workshops, RISC-V International is changing the way the industry works together and collaborates – creating a new kind of open hardware and software ecosystem. Become a member today and help pioneer the industry’s future de facto ISA for design innovation.

* Rust Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://foundation.rust-lang.org">https://foundation.rust-lang.org</a>
    * The Rust Foundation is an independent non-profit organization to steward the Rust programming language and ecosystem, with a unique focus on supporting the set of maintainers that govern and develop the project.

<a name="s"></a>
* seL4 Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://sel4.systems/Foundation/About/">https://sel4.systems/Foundation/About/</a>
    * seL4 Foundation forms an open, transparent and neutral organisation tasked with growing the seL4 ecosystem. It brings together developers of the seL4 kernel, developers of seL4-based components and frameworks, and those adopting seL4 in real-world systems. Its focus is on coordinating, directing and standardising development of the seL4 ecosystem in order to reduce barriers to adoption, raising funds for accelerating development, and ensuring clarity of verification claims.

* Shuttleworth Foundation
    * Type: Independent
    * Legal/financial status: South African Nonprofit
    * <a href="https://www.shuttleworthfoundation.org/">https://www.shuttleworthfoundation.org/</a>
    * The Shuttleworth Foundation is a small social investor that provides funding to dynamic leaders who are at the forefront of social change. We look for social innovators who are helping to change the world for the better and could benefit from a social investment model with a difference. We identify amazing people, give them a fellowship grant, and multiply the money they put into their own projects by a factor of ten or more. The Foundation is at its core an experiment in open philanthropy and uses alternative funding methodologies and collaborative ways of working to ensure that every fellow receives the necessary support to succeed.

* Signal Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://signalfoundation.org/">https://signalfoundation.org/</a>
    * Signal Foundation's mission is to develop open source privacy technology that protects free expression and enables secure global communication.

* SODA Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://sodafoundation.io">https://sodafoundation.io</a>
    * SODA Foundation is an open source project under Linux Foundation that aims to foster an ecosystem of open source data management and storage software for data autonomy. SODA Foundation offers a neutral forum for cross-projects collaboration and integration and provides end users quality end-to-end solutions.

* Software Freedom Conservancy
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://sfconservancy.org/">https://sfconservancy.org/</a>
    * Software Freedom Conservancy is a not-for-profit organization that helps promote, improve, develop, and defend Free, Libre, and Open Source Software (FLOSS) projects. Conservancy provides a non-profit home and infrastructure for FLOSS projects. This allows FLOSS developers to focus on what they do best — writing and improving FLOSS for the general public — while Conservancy takes care of the projects' needs that do not relate directly to software development and documentation.

* Software Freedom Law Center
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://www.softwarefreedom.org/">http://www.softwarefreedom.org/</a>
    * SFLC helps FOSS projects develop and maintain legal status to help ensure their longevity. SFLC assists its clients with all stages of corporate existence, including formation and tax exemption, and helps projects with their contracts and governance.

* Software in the Public Interest
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.spi-inc.org/">https://www.spi-inc.org/</a>
    * Software in the Public Interest (SPI) is a non-profit corporation registered in the state of New York founded to act as a fiscal sponsor for organizations that develop open source software and hardware. Our mission is to help substantial and significant open source projects by handling their non-technical administrative tasks so that they aren't required to operate their own legal entity.

* Software Libre Argentina
    * Type: Independent
    * Legal/financial status: Unknown
    * <a href="https://www.facebook.com/pg/SolarArg/about/?ref=page_internal">https://www.facebook.com/pg/SolarArg/about/?ref=page_internal</a>
    * SoLAr es una asociación civil que busca desde 2003 dar marco institucional a las acciones de la comunidad de militantes del Software Libre en la Argentina.

* Software Livre Brasil
    * Type: Independent
    * Legal/financial status: Brazilian NGO
    * <a href="http://www.softwarelivre.org/">http://www.softwarelivre.org/</a>
    * O Projeto Software Livre Brasil é uma rede de pessoas, mantida pela Associação Software Livre.org, que reúne universidades, empresários, poder público, grupos de usuários, hackers, ONG's e ativistas pela liberdade do conhecimento. Temos como objetivo a promoção do uso e do desenvolvimento do software livre como uma alternativa de liberdade econômica, tecnológica e de expressão.

* Southern California Linux Expo (SCALE)
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.socallinuxexpo.org/">https://www.socallinuxexpo.org/</a>
    * SCALE's mission is to provide educational opportunities on the topic of Open Source software. Open Source software is any software that meets the litmus test of the OSI (Open Source Initiative). Examples of OSS are GNU/Linux and the various BSD operating systems, and applications such as LibreOffice and Firefox.

<a name="t"></a>
* TARS Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://tarscloud.org">https://tarscloud.org</a>
    * The TARS Foundation is an open source microservice foundation to support the rapid growth of contributions and membership for a community focused on building an open microservices platform.

* TeX Users Group
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://tug.org/">http://tug.org/</a>
    * The TeX Users Group (TUG) is a membership-based not-for-profit organization founded in 1980, for those who are interested in typography and font design, and/or are users of the TeX typesetting system created by Donald Knuth.

* The Document Foundation
    * Type: Independent
    * Legal/financial status: German e.V.
    * <a href="https://www.documentfoundation.org">https://www.documentfoundation.org</a>
    * The Document Foundation is proud to be the home of LibreOffice, the next evolution of the world's leading free office suite, and The Document Liberation Project, a community of developers united to free users from vendor lock-in of content by providing powerful tools for the conversion of proprietary file formats to the corresponding ODF format.

* The Perl Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://perlfoundation.org/">https://perlfoundation.org/</a>
    * The Foundation accepts donations from organizations that depend on Perl or Raku. From individuals who love programming with the Perl or Raku languages. Our desire is to build a strong, healthy and sustainable language ecosystem and community.

* TODO Group
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://todogroup.org">https://todogroup.org</a>
    * TODO is an open group of companies who want to collaborate on practices, tools, and other ways to run successful and effective open source projects and programs.

* Tor Project, Inc.
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.torproject.org/about/history/">https://www.torproject.org/about/history/</a>
    * We, at the Tor Project, fight every day for everyone to have private access to an uncensored internet, and Tor has become the world's strongest tool for privacy and freedom online.

* Trust Over IP Foundation
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://trustoverip.org">https://trustoverip.org</a>
    * The Trust over IP Foundation is defining a complete architecture for Internet-scale digital trust that combines both cryptographic trust at the machine layer and human trust at the business, legal, and social layers.

* Twisted Software Foundation
    * Type: Fiscal sponsored (Software Freedom Conservancy)
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://twistedmatrix.com/trac/wiki/TwistedSoftwareFoundation">https://twistedmatrix.com/trac/wiki/TwistedSoftwareFoundation</a>
    * The name "Twisted Software Foundation" is a reference of convenience to our legal non-profit status through the Software Freedom Conservancy. The Conservancy has allowed us to pool organizational resources with other projects, such as Inkscape, Samba, and Wine, in order to reduce the management overhead associated with creating our own, dedicated legal entity.

* TYPO3 Association
    * Type: Independent
    * Legal/financial status: Swiss Nonprofit
    * <a href="https://typo3.org/project/association/">https://typo3.org/project/association/</a>
    * The TYPO3 Association builds the groundwork for the TYPO3 project. We empower our community with infrastructure and guidance. Our decisions are long term oriented.

<a name="u"></a>
* US GOV OPS
    * Type: Sub-organisation of The Linux Foundation
    * Legal/financial status: United States 501(3)(6)
    * <a href="https://usgovops.org">https://usgovops.org</a>
    * Allows United States Government projects, their ecosystem, and open community to participate in accelerating innovation and security in the areas of 5G, Edge, AI, Standards, Programmability, and IOT among other technologies. The project formation encourages ecosystem players to support US Government initiatives to create the latest in technology software.

<a name="v"></a>
<a name="w"></a>
* Wikimedia Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.wikimediafoundation.org">https://www.wikimediafoundation.org</a>
    * The Wikimedia Foundation is the nonprofit that hosts Wikipedia and our other free knowledge projects. We want to make it easier for everyone to share what they know. To do this, we keep Wikipedia and Wikimedia sites fast, reliable, and available to all. We protect the values and policies that allow free knowledge to thrive. We build new features and tools to make it easy to read, edit, and share from the Wikimedia sites. Above all, we support the communities of volunteers around the world who edit, improve, and add knowledge across Wikimedia projects.

* Wikiotics Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://wikiotics.org/en/Wikiotics_Foundation">https://wikiotics.org/en/Wikiotics_Foundation</a>
    * The Wikiotics Foundation is a nonprofit organization dedicated to making an interactive language instruction system that is freely licensed and freely available to anyone online. Our major activities include developing custom free software for collaborative language instruction and running the https://wikiotics.org community site where teachers and students from all over the world can come together to build high quality language instruction materials.

* WordPress Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://wordpressfoundation.org/">https://wordpressfoundation.org</a>
    * The WordPress Foundation is a charitable organization founded by Matt Mullenweg to further the mission of the WordPress open source project.

<a name="x"></a>
* X.Org Foundation
    * Type: Fiscal sponsored (SPI)
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.x.org/wiki/XorgFoundation">https://www.x.org/wiki/XorgFoundation</a>
    * X.Org Foundation's (or X.Org for short) purpose is to research, develop, support, organize, administrate, standardize, promote, and defend a free and open accelerated graphics stack and the developers and users thereof. This stack includes, but is not limited to, the following projects: DRM, Mesa, Wayland and the X Window System.

* Xiph.org
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://xiph.org/">https://xiph.org/</a>
    * The Xiph.Org Foundation is a non-profit corporation dedicated to protecting the foundations of Internet multimedia from control by private interests. Our purpose is to support and develop free, open protocols and software to serve the public, developer and business markets.

* XMPP Standards Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://xmpp.org/about/xmpp-standards-foundation.html">https://xmpp.org/about/xmpp-standards-foundation.html</a>
    * The XMPP Standards Foundation (also known as the XSF and formerly the Jabber Software Foundation) is an independent, nonprofit standards development organisation whose primary mission is to define open protocols for presence, instant messaging, and real-time communication and collaboration on top of the IETF’s Extensible Messaging and Presence Protocol (XMPP).

<a name="y"></a>
<a name="z"></a>
* Zig Software Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://ziglang.org/zsf/">https://ziglang.org/zsf/</a>
    * The mission of the Zig Software Foundation is to promote, protect, and advance the Zig programming language, to support and facilitate the growth of a diverse and international community of Zig programmers, and to provide education and guidance to students, teaching the next generation of programmers to be competent, ethical, and to hold each other to high standards.

* Zope Foundation
    * Type: Independent
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://foundation.zope.org/">https://foundation.zope.org/</a>
    * The Zope Foundation has the goal to promote, maintain, and develop the Zope platform. It does this by supporting the Zope community. Our community includes the open source community of contributors to the Zope software, contributors to the documentation and web infrastructure, as well as the community of businesses and organizations that use Zope. The Zope Foundation is the copyright holder of the Zope software and many extensions and associated software. The Zope Foundation also manages the zope.org website, and manages the infrastructure for open source collaboration.

## Defunct Organisations

These are organisations that may have closed up operations.

* Dojo Foundation
    * Type: Defunct (joined OpenJS Foundation)
    * Legal/financial status: United States 501(c)(6)
    * <a href="http://dojofoundation.org/">http://dojofoundation.org/</a>
    * Operating as a 501(c)(6) non-profit foundation, we work hard to provide just enough foundation to make great open-source projects succeed and be professional and trustworthy to their users, without bureaucracy or excessive process for projects and their contributors.

* El Centro de Software Libre
    * Type: Defunct
    * Legal/financial status: Unknown
    * <a href="http://www.csol.org/">http://www.csol.org/</a>
    * El Centro de Software Libre (CSOL) tiene como misión fomentar un ecosistema sustentable para las tecnologías y conocimiento libres en Chile y América Latina.

* Free Software and Open Source Foundation for Africa
    * Type: Defunct
    * Legal/financial status: Unknown
    * <a href="http://www.fossfa.net/">http://www.fossfa.net/</a>
    * FOSSFA is the premier African FOSS organization, and was founded under the auspices of the Bamako Bureau of the African Information Society Initiative within the mandate given by African Governments in 1995 to the United Nations Economic Commission for Africa (UNECA). The Vision of FOSSFA is to promote the use of FOSS and the FOSS model in African development, and the organization supports the integration of FOSS in national policies. FOSSFA also coordinates, promotes, and adds value to African FOSS initiatives, creativity, industry, expertise, efforts and activities at all levels.

* Linux Fund
    * Type: Defunct
    * Legal/financial status: United States 501(c)(3)
    * Linux Fund is a 501(c)(3) nonprofit organization that provides financial and advisory support to the free and open software community. Linux Fund has given away over 3/4 million dollars to open source events and development since its founding in 1999 using funds earned by its line of rewards credit cards and direct donations.

* Linux International
    * Type: Defunct
    * Legal/financial status: United States 501(c)(3)
    * <a href="https://www.li.org/">http://www.li.org/</a>
    * Linux International, also known as LI, is a world-wide non-profit association of end users who are dedicated to furthering the acceptance and use of Free and Open Source Software (FOSS).

* LogiLogi Foundation
    * Type: Defunct
    * Legal/financial status: Dutch Stichting
    * <a href="https://foundation.logilogi.org/">http://foundation.logilogi.org/</a>
    * The LogiLogi Foundation wants to create interesting web-sites and communities that are truly yours.

* Mambo Foundation, Inc.
    * Type: Defunct
    * Legal/financial status: Australian Nonprofit
    * <a href="http://about.mambo-foundation.org/">http://about.mambo-foundation.org/</a>
    * The Mambo Foundation is a non-profit corporation registered in Victoria, Australia. The Foundation provides organisational, financial, infrastructure and legal support for the Mambo open source software project. The Mambo Foundation is membership-based and was formed to ensure that Mambo continues to develop beyond the participation of individual volunteers.

* Open Hardware Foundation
    * Type: Defunct
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://www.openhardwarefoundation.org/">http://www.openhardwarefoundation.org/</a>
    * Donations allow the OHF to support open hardware projects, free software, and open source causes and the Foundation's initial focus is to facilitate production of the OGD1 hardware prototype card. Yes, your donations can help assist our OGP graphic card developers to purchase and develop on an ODG1 development board. There is a bright yellow donate button on the OHF homepage. If you want to help toward building an open graphics card please use it frequently.

* Open Health Tools
    * Type: Defunct
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.openhealthtools.org">https://www.openhealthtools.org</a>
    * Open Health Tools is an open source community with a vision of enabling a ubiquitous ecosystem where members of the Health and IT professions can collaborate to build interoperable systems that enable patients and their care providers to have access to vital and reliable medical information at the time and place it is needed. Open Health Tools will generate a vibrant active ecosystem involving software product and service companies, medical equipment companies, health care providers, insurance companies, government health service agencies, and standards organizations.

* Open Media Now! Foundation
    * Type: Defunct
    * Legal/financial status: United States 501(c)(6)
    * <a href="https://www.openmedianow.org">https://www.openmedianow.org</a>
    * OMNow supports free ways to create, to distribute and to display digital creative content. We are dedicated to the development, support and empowerment of an open media infrastructure for companies and individuals who desire more control over product technology and content. Our foundation opens the media market by actively developing operating system-agnostic and cross-platform solutions, and by conducting legislative research on media-related topics.

* Open Source Software Institute
    * Type: Defunct
    * Legal/financial status: United States 501(c)(6)
    * <a href="http://www.oss-institute.org/">http://www.oss-institute.org/</a>
    * The Open Source Software Institute (OSSI) is a non-profit (501(c)(6) organization comprised of corporate, government, academic and Open Source Community representatives whose mission is to promote the development and implementation of open-source software solutions within U.S. federal, state and municipal government agencies and academic entities.

* Parrot Foundation
    * Type: Defunct
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://www.parrot.org/foundation/">http://www.parrot.org/foundation/</a>
    * Parrot Foundation is a US (Washington State) non-profit foundation, incorporated to: Protect the intellectual property for the Parrot virtual machine, tools, libraries, and language implementations, while offering it for use under an open source license. Cultivate an open source community and encourage the growth of an ecosystem of tools, libraries, extensions, applications, and language implementations around Parrot. Support the development of Parrot and other Parrot community activities through events and grants, and provide essential technical, legal, and organizational infrastructure for the project.

* Peer-Directed Projects Center (freenode)
    * Type: Defunct
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://freenode.net/pdpc.shtml">http://freenode.net/pdpc.shtml</a>
    * freenode provides discussion facilities for the Free and Open Source Software communities, for not-for-profit organizations and for related communities and organizations. In 1998, the network had about 200 users and less than 20 channels. Ten years down the line the network currently peaks at just over 50,000 users, freenode provides facilities to a variety of groups and organizations.

* PostgreSQL Brasil
    * Type: Defunct
    * Legal/financial status: Brazilian NGO
    * <a href="http://postgresql.org.br">http://postgresql.org.br</a>
    * This is one of several foundations which support the PostgreSQL project. PostgreSQL Brasil is an organization which holds several educational conferences and events related to the PostgreSQL database management system.

* Public Software Fund
    * Type: Defunct
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://www.pubsoft.org/">http://www.pubsoft.org/</a>
    * The Public Software Fund helps patrons of the software arts to fund software projects. First, by being a charitable public foundation with a charter of creating more public software, so that donations are tax-deductible. Second, by helping donors find implementors, and vice-versa. Third, by aggregating funding from multiple donors.

* Software Libre Chile
    * Type: Defunct
    * Legal/financial status: Unknown
    * <a href="http://www.softwarelibre.cl/">http://www.softwarelibre.cl/</a>
    * Centro de conocimiento e información acerca del avance del Movimiento del Software y el Conocimiento Libre bajo una dinámica de construcción colaborativa.

* Subversion Corporation
    * Type: Defunct
    * Legal/financial status: United States 501(c)(6)
    * <a href="http://subversion.org/">http://subversion.org/</a>
    * The Subversion Corporation is a non-profit stewardship organization formed to hold and defend the copyrights and trademarks of the open source version control software Subversion®.

* The Software Conservancy
    * Type: Defunct
    * Legal/financial status: United States 501(c)(3)
    * <a href="http://www.tsc.org/">http://www.tsc.org/</a>
    * The Software Conservancy is a nonprofit organization incorporated under the laws of the State of California. Its primary role is to serve as an independent, neutral organization to hold copyright to open source or free software source code and to fulfill related functions with respect to public software development projects creating such code.

## Definitions

**Organisation types**

* **Umbrella**: An umbrella organisation acts as the organisational home for sub-organisations, potentially among other roles (such as the organisational home for projects and communities). 
* **Sub-organisation**: An organisation that has an Umbrella organisation as its parent organisation.
* **Fiscal sponsored**: An organisation that relies upon another non-profit for some of its operations.
* **Independent**: A "free standing" organisation, without sub-organisations or a parent organisation.

**Legal/financial statuses**

* [Argentinian NGO](https://www.cof.org/content/nonprofit-law-argentina)
* [Australian Nonprofit](https://www.cof.org/content/nonprofit-law-australia)
* [Brazilian NGO](https://www.cof.org/content/nonprofit-law-brazil)
* [Canadian Nonprofit](https://www.cof.org/content/nonprofit-law-canada)
* [Dutch Stichting](https://en.wikipedia.org/wiki/Stichting)
* [Dutch Vereniging](https://business.gov.nl/starting-your-business/choosing-a-business-structure/association/)
* [French 1901 Non-profit Association](https://www.cof.org/content/nonprofit-law-france)
* [German e.V.](https://www.cof.org/country-notes/nonprofit-law-germany)
* [Indian Nonprofit](https://www.cof.org/country-notes/nonprofit-law-india)
* Italian Non-Profit Association
* [Japanese Non-Profit Corporation](https://www.cof.org/content/nonprofit-law-japan)
* [Netherlands ANBI](https://en.wikipedia.org/wiki/Algemeen_nut_beogende_instelling)
* [South African Nonprofit](https://www.cof.org/country-notes/nonprofit-law-south-africa)
* [Swiss Nonprofit](https://www.mandint.org/en/guide-ngos)
* [United Kingdom not-for-profit limited by guarantee](https://www.cof.org/country-notes/nonprofit-law-england-wales)
* [United States 501(c)(3)](https://en.wikipedia.org/wiki/501(c)_organization#501(c)(3))
* [United States 501(c)(4)](https://en.wikipedia.org/wiki/501(c)_organization#501(c)(4))
* [United States 501(c)(6)](https://en.wikipedia.org/wiki/501(c)_organization#501(c)(6))

