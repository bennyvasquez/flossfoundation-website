---
layout: single
title: "European Union NGO Resources"
permalink: /european-union-ngo-resources
---

This is a rough set of resources that I have accumulated with regards to NGO and especially cross-border issues in the European Union. I am happy to answer questions on this topic: dexter@ambidexter.com

A basic introduction to some of the issues involved:
http://www.givingineurope.org

A taste of the issues and progress on them: Persche Case: January 2009
http://www.givingineurope.org/site/index.cfm?TID=2&BID=1&SID=1&LG=2&ART=...

European Foundation Centre
http://www.efc.be/

Challenges and opportunities for foundations’ and funders’ work across borders
http://www.efc.be/projects/eu/legal/challenges.htm

Lobby for better VAT status for NGO's
http://www.eccvat.org/

List of European gift taxes:
http://www.agn-europe.org/htm/firm/news/ttf/2009/09inheritance_broch.pdf

European National NGO Umbrellas
http://www.cedag-eu.org/

More or less a cross-border umbrella organization:
http://www.transnationalgiving.eu/

Any cross-border giving agreement will probably be a sub-set of a cross-border income-tax agreement. These exist between many countries.

DE Double Taxation Prevention Treaties
http://www.worldwide-tax.com/germany/ger_double.asp

NL Double Taxation Prevention Treaties (76 countries)
http://www.strik-law.nl/dokjes/handouts/dutch_tax_treaties.pdf

IBFD is a leading international provider of cross-border tax expertise and independent tax research.
http://www.ibfd.org/

The Netherlands tax authorities are unique in that they will recognize registered foreign public-benefit organizations. The questions are quite strait-forward and I have the application form plus a Google-translated version of it. I am warned that as this is a new program, the scrutiny may increase over time.

http://www.belastingdienst.nl/variabel/buitenland/en/private_taxpayers/p...

The appropriate tax office:

```
Belastingdienst 's-Hertogenbosch
T.a.v. ANBI
Postbus 70505
5201 CG 's-Hertogenbosch
+31 73 6245848
```

The NL NGO portal:
www.anbi.nl

An NL NGO lawyer:

```
Ineke A. Koele PhD
Benvalor Attorneys at law - Tax lawyers
Wilhelminapark 60-61
3581 NP Utrecht
the Netherlands
t: +31 (0) 88 - 30 300 30
f: +31 (0) 88 - 30 300 33
w: www.benvalor.com
```
